#ifndef PMODALS_H
#define PMODALS_H

#include "SpiDevice.h"

namespace PMOD
{
    uint32_t getAlsValidSpeed(const uint32_t& s);

    class PmodAls
    {
    public:
        PmodAls(const uint32_t& periph, const uint32_t& cs, const uint32_t& s = 1000000/*1 MHz*/);
        virtual ~PmodAls() = default;

        bool isPeripheralOK() const {return device.isPeripheralOK();}

        bool readAmbiantLight(uint16_t& value);

        PmodAls(const PmodAls&) = delete;  // copy constructor
        PmodAls& operator=(const PmodAls&) = delete;  // copy assignment
        PmodAls(PmodAls&&) = delete;  // move constructor
        PmodAls& operator=(PmodAls&&) = delete;  // move assignment

    private:
        OSEF::SpiDevice device;
    };
}  // namespace PMOD

#endif /* PMODALS_H */
