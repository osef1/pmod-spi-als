#include "PmodAls.h"
#include "SignalHandler.h"
#include "TimeOut.h"

#include <getopt.h>  // getopt
#include <iostream>  // cout

bool getSpiPeripheralChip(int argc, char** argv, uint32_t& periph, uint32_t& cs)
{
    bool ret = true;

    if (argc > 1)
    {
        uint64_t tmp = 0;
        int32_t opt = getopt(argc, argv, "p:c:");
        while (opt != -1)
        {
            switch (opt)
            {
                case 'p':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if (tmp <= 0xffUL)
                    {
                        periph = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
                    break;
                case 'c':
                    tmp = std::strtoul(optarg, nullptr, 10);
                    if (tmp <= 0xffUL)
                    {
                        cs = tmp;
                    }
                    else
                    {
                        ret = false;
                    }
                    break;
                default:
                    ret = false;
                    break;
            }

            opt = getopt(argc, argv, "p:c:");
        }
    }

    if (not ret)
    {
        std::cout << "Usage:   " << argv[static_cast<size_t>(0)] << " [-option] [argument]" << std::endl;
        std::cout << "option:  " << std::endl;
        std::cout << "         " << "-p  SPI peripheral [0-255]" << std::endl;
        std::cout << "         " << "-c  SPI chip select [0-255]" << std::endl;
    }

    return ret;
}

int main(int argc, char** argv)
{
    int32_t ret = -1;

    OSEF::SignalHandler signal;
    if (signal.setSignalActionList({SIGINT, SIGTERM}))
    {
        uint32_t periph = 0;
        uint32_t chip = 0;
        if (getSpiPeripheralChip(argc, argv, periph, chip))
        {
            PMOD::PmodAls als(periph, chip);

            uint16_t cl = 0;
            uint16_t pl = 0;
            do
            {
                if (als.readAmbiantLight(cl))
                {
                    if (cl != pl)
                    {
                        pl = cl;
                        std::cout << cl << std::endl;
                    }
                    ret = 0;
                }
                else
                {
                    ret = -1;
                }

                OSEF::sleepms(100);
            }while (als.isPeripheralOK() && not signal.signalReceived() && (ret == 0));
        }
    }

    return ret;
}
