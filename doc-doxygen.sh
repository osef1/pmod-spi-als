#!/bin/bash

mkdir -p doc/doxygen

cd doc/doxygen

doxygen --version

doxygen ../../doxyfile
