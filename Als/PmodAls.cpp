#include "PmodAls.h"
#include "Debug.h"

PMOD::PmodAls::PmodAls(const uint32_t& periph, const uint32_t& cs, const uint32_t& s)
    :device({periph, cs}, 0, getAlsValidSpeed(s)) {}

bool PMOD::PmodAls::readAmbiantLight(uint16_t& value)
{
    const bool ret = device.readWordSwap(value);
    return ret;
}

uint32_t PMOD::getAlsValidSpeed(const uint32_t& s)
{
    uint32_t ret = 0;

    if (s < 1000000U)
    {
        ret = 1000000U;
        DERR("SPI speed must be between 1 and 4 MHz, not " << s << ". 1 MHz will be used instead");
    }
    else if (s > 4000000U)
    {
        ret = 4000000U;
        DERR("SPI speed must be between 1 and 4 MHz, not " << s << ". 4 MHz will be used instead");
    }
    else
    {
        ret = s;
    }

    return ret;
}
